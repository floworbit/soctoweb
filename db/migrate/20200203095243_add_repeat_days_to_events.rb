class AddRepeatDaysToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :repeat_days, :text, array:true
  end
end
