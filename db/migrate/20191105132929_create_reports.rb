class CreateReports < ActiveRecord::Migration[5.1]
  def change
    create_table :reports do |t|
      t.references :event_message, foreign_key: true
    end
  end
end
