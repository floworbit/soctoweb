class CreateRoomMessage < ActiveRecord::Migration[5.1]
  def change
    create_table :event_messages do |t|
      t.references :event, foreign_key: true
      t.references :user, foreign_key: true
      t.text :message
    end
  end
end
