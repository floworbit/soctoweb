class AddMuteToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :muted_until, :datetime
  end
end
