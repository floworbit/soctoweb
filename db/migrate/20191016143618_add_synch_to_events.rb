class AddSynchToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :synch_event, :boolean
    add_column :events, :starting_time, :datetime
  end
end
