class CreateBlock < ActiveRecord::Migration[5.1]
  def change
    create_table :blocks do |t|
      t.references :user, foreign_key: true
      t.integer :target
    end
  end
end