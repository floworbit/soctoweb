class RenameSaleToPurchase < ActiveRecord::Migration[5.1]
  def change
    drop_table :sales

    create_table :purchases do |t|
      t.boolean :download_access
      t.string :watermark
      t.integer :paid
      t.datetime :date
      t.references :conference
      t.references :user

      t.timestamps
    end
  end
end
