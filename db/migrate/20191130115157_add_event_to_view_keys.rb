class AddEventToViewKeys < ActiveRecord::Migration[5.1]
  def change
    add_column :view_keys, :event_id, :int
  end
end
