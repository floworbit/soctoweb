class RemoveForeignKeyFromArchivedNames < ActiveRecord::Migration[5.1]
  def change
    remove_column :archived_names, :user_id, :integer
    add_column :archived_names, :user_id, :integer
  end
end
