class MoveGroupEventsToOwnTable < ActiveRecord::Migration[5.1]
  def change
    remove_column :events, :join_code, :string

    execute "CREATE TABLE group_events (LIKE events INCLUDING DEFAULTS INCLUDING INDEXES);"

    add_column :group_events, :join_code, :string
  end
end
