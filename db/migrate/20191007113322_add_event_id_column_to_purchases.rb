class AddEventIdColumnToPurchases < ActiveRecord::Migration[5.1]
  def change
    add_column :purchases, :event_id, :integer
  end
end
