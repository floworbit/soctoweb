class AddOriginalEventToGroupEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :group_events, :original_event, :integer
  end
end
