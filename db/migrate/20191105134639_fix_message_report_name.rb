class FixMessageReportName < ActiveRecord::Migration[5.1]
  def change
    rename_table :reports, :message_reports
  end
end
