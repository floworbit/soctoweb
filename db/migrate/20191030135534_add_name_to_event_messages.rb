class AddNameToEventMessages < ActiveRecord::Migration[5.1]
  def change
    add_column :event_messages, :name, :string
  end
end
