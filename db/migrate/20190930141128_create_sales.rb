class CreateSales < ActiveRecord::Migration[5.1]
  def change
    create_table :sales do |t|
      t.boolean :download_access
      t.string :watermark
      t.integer :paid
      t.datetime :date
      t.references :conference, foreign_key: true
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
