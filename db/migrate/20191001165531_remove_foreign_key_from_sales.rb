class RemoveForeignKeyFromSales < ActiveRecord::Migration[5.1]
  def change
    remove_column :sales, :user_id, :integer
    add_column :sales, :user_id, :integer
  end
end
