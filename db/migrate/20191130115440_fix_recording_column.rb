class FixRecordingColumn < ActiveRecord::Migration[5.1]
  def change
    remove_column :view_keys, :event_id
    add_column :view_keys, :filename, :string
  end
end
