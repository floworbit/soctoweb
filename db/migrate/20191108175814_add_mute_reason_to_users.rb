class AddMuteReasonToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :mute_reason, :string
  end
end
