class AddFinalizedColumnToPurchases < ActiveRecord::Migration[5.1]
  def change
    remove_column :purchases, :date, :datetime
    add_column :purchases, :finalized, :boolean
    add_column :purchases, :paypal_id, :string
  end
end
