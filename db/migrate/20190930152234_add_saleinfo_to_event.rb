class AddSaleinfoToEvent < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :supportable, :boolean
    add_column :events, :view_threshold, :integer
    add_column :events, :downloadable, :boolean
    add_column :events, :download_threshold, :integer
  end
end
