class AddGroupEventToEvents < ActiveRecord::Migration[5.1]
  def change
    add_column :events, :join_code, :string
    add_column :events, :type, :string
    create_table :group_members do |t|
      t.integer :event_id
      t.integer :user_id
      t.timestamps
    end
  end
end
