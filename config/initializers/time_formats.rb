# config/initializers/time_formats.rb
Time::DATE_FORMATS[:short_german] = "%d.%m.%Y um %H:%M"
Time::DATE_FORMATS[:date_german] = "%d.%m.%Y"
Time::DATE_FORMATS[:time_german] = "%H:%M"