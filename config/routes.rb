Rails.application.routes.draw do
  devise_for :users, :path => '', :path_names => { :sign_in => "login", :sign_out => "logout", :sign_up => "register" }, controllers: { registrations: 'users/registrations' }
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  #root 'admin/dashboard#index'

  # VOC JSON API
  namespace :api do
    resources :conferences, :defaults => { :format => 'json' }
    resources :events, :defaults => { :format => 'json' } do
      collection do
        post 'update_promoted'
        post 'update_view_counts'
      end
    end
    resources :recordings, :defaults => { :format => 'json' }
    resources :news, :defaults => { :format => 'json' }
  end

  # PUBLIC JSON API
  namespace :public do
    get :index, path: '/', defaults: { format: 'json' }
    get :oembed
    resources :conferences, only: [:index, :show], defaults: { format: 'json' }
    constraints(id: %r'[^/]+') do
      resources :events, only: %i(index show), defaults: { format: 'json' } do
        get :search, defaults: { format: 'json' }, on: :collection
      end
    end
    resources :recordings, only: %i(index show), defaults: { format: 'json' } do
      collection do
        post 'count'
      end
    end
  end

  # GRAPHQL
  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end
  post "/graphql", to: "graphql#execute"

  # CONTENT
  get '/content/*path', to: 'content#load'

  # FRONTEND
  scope module: 'frontend' do
    root to: 'home#index'
    if Rails.env.production?
      get '404', to: 'home#page_not_found'
    end
    get '/about', to: 'home#about'
    get '/impressum', to: 'home#impressum'
    get '/agb', to: 'home#agb'
    get '/datenschutz', to: 'home#datenschutz'
    get '/search', to: 'search#index'
    get '/sitemap.xml', to: 'sitemap#index', defaults: { format: 'xml' }

    get '/group_event', to: 'account#group'
    get '/library', to: 'account#library'
    get '/settings', to: 'account#settings'
    get '/settings/gdpr-download', to: 'account#gdpr_download'
    get '/settings/blocks', to: 'account#blocks'

    get '/v/:slug', to: 'events#show', as: :event, :constraints => { slug: %r'[^/]+' }
    get '/postroll/:slug', to: 'events#postroll', as: :postroll, :constraints => { slug: %r'[^/]+' }
    #get '/v/:slug/oembed', to: 'events#oembed', as: :oembed_event, :constraints => { slug: %r'[^/]+' }
    get '/v/:slug/playlist', to: 'events#playlist_conference', as: :playlist_conference, :constraints => { slug: %r'[^/]+' }
    get '/v/:slug/audio', to: 'events#audio_playlist_conference', as: :audio_playlist_conference, :constraints => { slug: %r'[^/]+' }
    get '/v/:slug/related', to: 'events#playlist_related', as: :playlist_related, :constraints => { slug: %r'[^/]+' }

    get '/g/:slug', to: 'group_event#show', :constraints => { slug: %r'[^/]+' }

    get '/c/:acronym', to: 'conferences#show', as: :conference

    get '/a', to: 'conferences#all', as: :all_conferences
    get '/b', to: 'conferences#browse', as: :browse_start
    get '/b/*slug', to: 'conferences#browse', as: :browse

    get '/recent', to: 'recent#index'
    get '/popular', to: 'popular#index'

    get '/tags/:tag', to: 'tags#show', as: :tag

    get '/purchase' => 'purchase#execute'

    get '/block' => 'block#getblocks'

    get '/moderation/:id/mute', to: 'moderation#mute_menu', as: :mute_menu, :constraints => { id: %r'[^/]+' }
    get '/moderation/:id/context', to: 'moderation#context', as: :context, :constraints => { id: %r'[^/]+' }
    match '/moderation/:id/unmute', to: 'moderation#unmute_menu', as: :unmute_menu, :constraints => { id: %r'[^/]+' }, :via => :post
    match '/moderation/:id/approve', to: 'moderation#approve', as: :approve, :constraints => { id: %r'[^/]+' }, :via => :post
    match '/moderation/:id/delete', to: 'moderation#delete', as: :delete, :constraints => { id: %r'[^/]+' }, :via => :post
    match '/moderation/:id/mute', to: 'moderation#mute', as: :mute, :constraints => { id: %r'[^/]+' }, :via => :post
    match '/join_group', to: 'group_event#join', as: :join, :via => :post

    resources :event_message, :only => [:create]
    resources :purchase, :only => [:create]
    resources :message_report, :only => [:create]
    resources :block, :only => [:create, :destroy]
    resources :group_event, :only => [:create]

  end

end
