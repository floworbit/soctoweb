ActiveAdmin.register User do
  index do
    column :id
    column :email
    column :name
    column "Muted" do |user|
      if user.muted_until != nil and user.muted_until > Time.zone.now
        "X"
      end
    end
    column :muted_until
    column :mute_reason

    actions defaults: false do |user|
      link_to "Unmute", "/moderation/#{user.id}/unmute", method: :post
    end
  end
end