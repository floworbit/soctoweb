ActiveAdmin.register_page 'Dashboard' do
  require 'sidekiq/api'

  menu :priority => 1, :label => proc { I18n.t('active_admin.dashboard') }

  content :title => proc { I18n.t('active_admin.dashboard') } do
    columns do
      column do
        panel 'Queued Jobs' do
          stats = Sidekiq::Stats.new
          columns do
            column { 'queued job count: ' + Sidekiq::ScheduledSet.new.count.to_s }
            column { 'processed jobs:' + stats.processed.to_s }
            column { 'failed jobs:' + stats.failed.to_s }
          end
        end

        panel "StartJobs" do
          scheduled_set = Sidekiq::ScheduledSet.new
          ul do
            scheduled_set.each do |scheduled|
              if scheduled.display_class == "EventStartJob"
                id = scheduled.display_args[0]
                li "Event " + id.to_s + ", '" + Event.find(id).title + "', " + Time.zone.at(scheduled.score).to_formatted_s(:short_german)
              end
            end
          end
        end

        panel 'Recent Conferences' do
          ul do
            Conference.recent(5).map do |conference|
              li link_to(conference.acronym, admin_conference_path(conference))
            end
          end
        end
      end
    end # columns
  end # content
end
