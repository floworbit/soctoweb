ActiveAdmin.register MessageReport do
  index do

    column "Event" do |report|
      Event.find(EventMessage.find(report.event_message.id).event_id).title
    end

    column "Name" do |report|
      report.event_message.name
    end

    column "Nachricht" do |report|
      report.event_message.message
    end

    actions defaults: false do |report|
      item "Context", "/moderation/#{report.event_message.id}/context"
    end

    actions defaults: false do |report|
      item "Mute User", "/moderation/#{report.event_message.id}/mute"
    end

    actions defaults: false do |report|
      item "Approve", "/moderation/#{report.id}/approve", method: :post
    end

    actions defaults: false do |report|
      item "Remove", "/moderation/#{report.id}/delete", method: :post
    end
  end
end