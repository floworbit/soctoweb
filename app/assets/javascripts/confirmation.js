$(document).ready(function(){
    const confirmationText = document.querySelector('#user_confirmation');
    const btn = document.getElementById("delete")
    $(btn).attr("disabled", "disabled");
    confirmationText.addEventListener('input', function(e) {
        if (confirmationText.value.localeCompare("Wirklich löschen!") === 0){
            $(btn).removeAttr("disabled");}
        else{
            $(btn).attr("disabled", "disabled");}
    })
});