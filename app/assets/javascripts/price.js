function checkPriceInput(threshold) {
    const btn = document.getElementById("buy");
    const input = document.getElementById("price_input").value
    $(btn).attr("disabled", "disabled");
    const regex = /^[1-9]\d*([.,]\d{0,2})?$/;
    const cleanValue = parseFloat(input.replace(",","."))*100
    if (regex.test(input) && cleanValue >= threshold) {
        $(btn).removeAttr("disabled");
    } else {
        $(btn).attr("disabled", "disabled");
    }
}