var MirrorbrainFix = {
  selectMirror: function(url, cb) {
    // TODO change this back
    // During development just give us the only mirror right away, we are not using a CDN
    // If we don't do this the Server will respond with the complete file instead of the expected JSON
    console.log('using mirror', url);
    cb(url);
    /**
    // Always request CDN via https
    url = url.replace(/^http:/, 'https:');
    //console.log('asking cdn for first mirror of', url);
    return $.ajax({
      url: url,
      dataType: 'json',
      success: function(dom) {
        var mirror = dom.MirrorList[0].HttpURL + dom.FileInfo.Path;
        console.log('using mirror', mirror);
        cb(mirror);
      }
    });
    **/
  }
}