//= require cable.js
var unmute = new Event('unmute');

$(function() {
    var stamp = window.location.hash.split('#t=')[1] || window.location.hash.split('&t=')[1],
        lang = (window.location.hash.split('#l=')[1] || '').split('&')[0],
        $video = $('video'),
        promises = [];

    $('video source').each(function() {
        var $source = $(this);
        promises.push(
            MirrorbrainFix.selectMirror($source.prop('src'), function(mirror) {
                $source.attr('src', mirror);
            })
        );
    });

    var isSynchEvent = $('.chat').length;
    var playerFeatures;
    if (!isSynchEvent) {
        playerFeatures = ['skipback', 'playpause', 'jumpforward', 'progress', 'current', 'duration',
            'tracks', 'volume', 'speed', 'sourcechooser', 'fullscreen', 'postroll', 'timelens']
    }else{
        playerFeatures = ['progress', 'current', 'duration', 'tracks', 'volume','sourcechooser', 'fullscreen', 'postroll', 'timelens']
    }

    $.when.apply($, promises).done(function() {
        $('video').mediaelementplayer({
            usePluginFullScreen: true,
            enableAutosize: true,
            stretching: '#{stretching}',
            features: playerFeatures,
            skipBackInterval: 15,
            startLanguage: lang,
            clickToPlayPause: !isSynchEvent,
            enableKeyboard: !isSynchEvent,
            success: function (mediaElement, originalNode, player) {
                $('[data-channel-subscribe="event_sync"]').each(function(index, element) {

                    if(isSynchEvent) {
                        mediaElement.muted = true;
                    }

                    var $element = $(element),
                        eventSyncId = $element.data('id');

                    App.cable.subscriptions.create(
                        {
                            channel: "EventSyncChannel",
                            event_sync: eventSyncId
                        },
                        {
                            received: function(data) {
                                if(data === 'start'){
                                    $('.starting-time').fadeOut();
                                    mediaElement.setCurrentTime(0);
                                    player.play();
                                    if(mediaElement.muted){
                                        document.getElementById("unmute-button").style.display = 'unset'; // if we still are muted show the button
                                    }
                                }else{
                                    let dataPassed = Number(data);
                                    let offset = (dataPassed - mediaElement.currentTime);

                                    if(offset > 6){
                                        mediaElement.setCurrentTime(dataPassed); // make a hard correction
                                    }
                                    else if(offset > 3){
                                        mediaElement.playbackRate = 1.103; // ~3.1s margin until the next signal arrives in 30s
                                    }else if(offset > 2){
                                        mediaElement.playbackRate = 1.07; // 2.1s
                                    }else{
                                        mediaElement.playbackRate = 1;
                                    }

                                    if((player.paused && !player.ended)){
                                        mediaElement.setCurrentTime(dataPassed);
                                        player.play();
                                        document.getElementById("syncing-notice").style.display = 'none';
                                        if(!mediaElement.muted){
                                            document.getElementById("unmute-button").style.display = 'none';
                                        }
                                    }
                                }
                            }
                        }
                    );
                });

                mediaElement.addEventListener('canplay', function () {
                    if (stamp) {
                        mediaElement.setCurrentTime(stamp);
                        stamp = null;
                    }
                });
                mediaElement.addEventListener('playing', function () {
                    $.post("/public/recordings/count", {event_id: $video.data('id'), src: mediaElement.src});
                }, false);

                function updateHash() {
                    var l = player.options.startLanguage || '';
                    var hash =  (l ? '#l='+l+'&t=' : '#t=') + Math.round(mediaElement.currentTime);
                    window.location.replaceHash(hash);
                }
                mediaElement.addEventListener('pause', updateHash, false);
                mediaElement.addEventListener('seeked', updateHash, false);
                document.addEventListener('unmute', function (event) {
                    mediaElement.muted = false;
                    mediaElement.setVolume(0.8);
                    document.getElementById("unmute-button").style.display = 'none';
                });
                document.addEventListener('keypress', function (event) {
                    // do not capture events from input fields
                    if (event.target.tagName === 'INPUT' || event.target.tagName === 'TEXTAREA' || isSynchEvent) {
                        return;
                    }
                    switch(event.key) {
                        case 'k':
                            player.paused ? player.play() : player.pause();
                            break;
                        case 'f':
                            player.fullscreen();
                            //event.preventDefault();
                            break;
                        default:
                            // forward other events to player
                            mediaElement.dispatchEvent(event);
                    }
                });
            }
        });
    });
});