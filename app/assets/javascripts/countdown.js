function dispatchUnmute(){
    document.dispatchEvent(unmute);
}

$(function() {
    document.addEventListener("mouseup", dispatchUnmute, true); //while we are waiting also unmute on user interaction so they dont have to do it by themselves
    var text= $('#minutes-left').text().replace(/\n/g,'');
    var countDownDate = new Date(text).getTime();
    var x = setInterval(function() {
        var now = new Date().getTime();
        var distance = countDownDate - now;
        if(distance < 3600000){
            clearInterval(x);
            document.removeEventListener("mouseup", dispatchUnmute, true);
        }else{
            var minutes = Math.ceil((distance % (1000 * 60 * 60)) / (1000 * 60));
            if (minutes !== 1) {
                $('#minutes-left').text(minutes + ' Minuten.');
            }else{
                $('#minutes-left').text(minutes + ' Minute.');
            }
        }
    },1000);
});