//= require cable.js

let blockedUsers;
let showBlockedMessages;
let vidCss;
let chatCss;

$(window).load(function(){
    showBlockedMessages = false;
    loadBlockedUsers();
    updateScrollPosition();
});

function toggleFullscreen() {
    function isFullscreen() {
        return $('#chat-video-wrapper').css('position') === 'absolute';
    }

    function on() {
        $('body').css({
            position: 'absolute',
            height: '100%',
            overflow: 'hidden',
        });

        $('.chat-video-wrapper').css({
            position: 'absolute',
            justifyContent: 'center',
            backgroundColor: '#000',
            width: $(window).width(),
            height: $(window).height(),
            marginLeft: 0,
            marginRight: 0,
            marginTop: 0,
            top: 0,
            left: 0,
            zIndex: 1040
        });

        vidCss = $('.video').css(["position", "height", "width"]);

        $('.video').css({
            position: 'absolute',
            width: '80vw',
            height: '100%',
            left: 0
        });

        chatCss = $('.chat').css(["position", "width"]);

        $('.chat').css({
            position: 'absolute',
            height: '100vh',
            width: '19vw',
            right: 0
        });
    }

    function off() {
        $('body').css({
            position: '',
            height: '',
            overflow: '',
        });

        $('.chat-video-wrapper').css({
            position: 'static',
            justifyContent: '',
            backgroundColor: '',
            width: '',
            height: '',
            marginLeft: '10vw',
            marginRight: '10vw',
            marginTop: '1vh',
            top: '',
            left: '',
            zIndex: ''
        });

        $('.video').css(vidCss);

        $('.chat').css(chatCss);
        $('.chat').css({
            height: ''
        });
    }

    $(window).resize(function () {
        if (isFullscreen()) {
            on();
        }
    });

    if (isFullscreen()) {
        off();
    } else {
        on();
    }
}

function submitOnEnter(evt, thisObj) {
    evt = (evt) ? evt : ((window.event) ? window.event : "")
    if (evt) {
        if ((evt.keyCode === 13 || evt.which === 13) && !evt.shiftKey) {
            $.ajax({
                type: "POST",
                url: "/event_message",
                data: $("#chat-form").serialize(), // serializes the form's elements.
                success: function(data)
                {
                    thisObj.value = null;
                }
            });
            return false;
        }
    }
}

function updateScrollPosition() {
    var out = document.getElementById("chat-messages");
    //var isScrolledToBottom = out.scrollHeight - out.clientHeight <= out.scrollTop + 1;
    //console.log(isScrolledToBottom);
    //if(isScrolledToBottom)
    out.scrollTop = out.scrollHeight - out.clientHeight;
}

$(function () {
    $('[data-channel-subscribe="event"]').each(function (index, element) {
        let $element = $(element),
            event_id = $element.data('event-id');

        App.cable.subscriptions.create(
            {
                channel: "EventChannel",
                event: event_id
            },
            {
                received: function (data) {
                    let messageTemplate;
                    if (data.name === document.getElementById("current_user").textContent.replace(/\s/g, '')) {
                        messageTemplate = $('[data-role="own-message-template"]');
                    } else {
                        messageTemplate = $('[data-role="message-template"]');
                    }
                    let content = messageTemplate.children().clone(true, true);
                    content.find('[data-role="message-id"]').text(data.id);
                    content.find('[data-role="user-id"]').text(data.user_id);
                    content.find('[data-role="user-name"]').text(data.name);
                    content.find('[data-role="message-text"]').text(data.message);
                    if (blockedUsers.some(item => item.target === data.user_id) && !showBlockedMessages) {
                        content.css("display", "none");
                    }
                    $element.append(content);
                    updateScrollPosition();
                    toggleBlockedMessages();
                }
            }
        );
    });
});

function report(element) {
    let message = $(element).parents(".chat-message")[0];
    let user = $(message).find(".message-username")[0].innerText;
    let text = $(message).find(".message-text")[0].innerText;
    let id = $(message).find(".message-id")[0].innerText;
    if (window.confirm(
        `Name: '${user}'
Nachricht: '${text}'

Diesen Beitrag melden?`)) {
        var http = new XMLHttpRequest();
        http.open('POST', '/message_report', true);
        http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        http.send(`id=${id}`);
        alert("Danke für deine Hilfe.");
    }
}

function loadBlockedUsers() {
    var http = new XMLHttpRequest();
    http.onreadystatechange = function () {
        if (http.readyState === XMLHttpRequest.DONE) {
            blockedUsers = JSON.parse(http.responseText);
            toggleBlockedMessages();
        }
    };
    http.open('GET', '/block', true);
    http.send(null);
}

function toggleBlockedMessages() {
    $("#chat-messages").find(".user-id").each(function () {
        if (blockedUsers != null && blockedUsers.some(item => item.target === Number(this.textContent.replace(/\s/g, '')))) {
            if (showBlockedMessages) {
                $(this).parents(".chat-message").css("display", "inherit")
            } else {
                $(this).parents(".chat-message").css("display", "none");
            }
        }
    });
}

function manualToggleBlockedMessages() {
    showBlockedMessages = !showBlockedMessages;
    toggleBlockedMessages();
    updateScrollPosition();
}

function block(element) {
    let message = $(element).parents(".chat-message")[0];
    let user = $(message).find(".message-username")[0].innerText;
    let id = $(message).find(".message-id")[0].innerText;
    let user_id = $(message).find(".user-id")[0].innerText;

    if (blockedUsers.some(item => item.target === Number(user_id.replace(/\s/g, '')))) {
        alert(`Du hast ${user} bereits blockiert. Um blockierte Nachrichten wieder zu verstecken klicke auf das Auge oben rechts.`)
    }else{
        if (window.confirm(`${user} blockieren?
        
Du wirst dann keine Nachrichten der Person angezeigt bekommen.`)) {
            var http = new XMLHttpRequest();
            http.onreadystatechange = function () {
                if (http.readyState === 4) {
                    if (http.status === 204) {
                        loadBlockedUsers();
                        alert(`Du hast ${user} blockiert.`);
                    }
                }
            };
            http.open('POST', '/block', true);
            http.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            http.send(`id=${id}`);
            toggleBlockedMessages();
            updateScrollPosition();
        }
    }
}

function mute(element) {
    let message = $(element).parents(".chat-message")[0];
    let id = $(message).find(".message-id")[0].innerText;
    window.open(`/moderation/${id}/mute`, "Moderieren", "height=300,width=500");
}