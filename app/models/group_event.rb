class GroupEvent < ApplicationRecord
  has_many :recordings, :foreign_key => :event_id, :primary_key => :original_event
  has_many :event_messages, dependent: :destroy, inverse_of: :event

  def thumb_url
    if thumb_filename_exists?
      File.join(Settings.static_url, Conference.find(conference_id).images_path, thumb_filename).freeze
    else
      conference.logo_url.freeze
    end
  end

  def thumb_filename_exists?
    return if thumb_filename.blank?
    true
  end
end