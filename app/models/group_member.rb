class GroupMember < ApplicationRecord

  def get_event
    GroupEvent.find(event_id)
  end
end