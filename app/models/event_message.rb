class EventMessage < ApplicationRecord
  belongs_to :user
  belongs_to :event, inverse_of: :event_messages
  has_many :message_report, dependent: :delete_all
end
