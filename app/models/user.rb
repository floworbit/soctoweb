class User < ApplicationRecord
  include ActiveModel::Validations

  has_many :event_message, dependent: :delete_all
  has_many :block, dependent: :delete_all

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  class NameValidator < ActiveModel::EachValidator
    def validate_each(record, attribute, value)
      record.errors.add attribute, "ist vergeben" unless (ArchivedName.where(name: value).blank? or !(ArchivedName.where(name: value, user_id: record.id).blank?)) and (User.where(name: value).blank? or !(User.where(name: value, id: record.id).blank?))
    end
  end

  validates :name, :length => { :maximum => 16, :minimum => 2}, :name => true

  def to_csv
    # cursed code, later this should be split into one file for each table, but it works for now
    csv = CSV.generate() do |csv|
      purchases = Purchase.where(user_id: id)
      archived_names = ArchivedName.where(user_id: id)

      csv << %w{email names muted_until created_at updated_at purchases}

      csv << [
          email,
          name,
          muted_until,
          created_at,
          updated_at,
          purchases[0].nil? ? "" : Event.find(purchases[0].event_id).title + "," + purchases[0].paid.to_s + "ct,"+ purchases[0].paypal_id
      ]

      purchases = purchases.drop(1)

      (0..[purchases.length, archived_names.length].max).each do |i|
        csv << [
            "",
            archived_names[i].nil? ? "" : archived_names[i].name,
            "",
            "",
            "",
            purchases[i].nil? ? "" : Event.find(purchases[i].event_id).title + "," + purchases[i].paid.to_s + "ct,"+ purchases[i].paypal_id
        ]
      end
    end
  end
end
