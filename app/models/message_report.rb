class MessageReport < ApplicationRecord
  belongs_to :event_message, dependent: :destroy
end
