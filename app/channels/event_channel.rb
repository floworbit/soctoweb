class EventChannel < ApplicationCable::Channel
  def subscribed
    stream_from "event_#{params[:event]}"
  end
end