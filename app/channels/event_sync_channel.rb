class EventSyncChannel < ApplicationCable::Channel
  def subscribed
    stream_from "event_sync_#{params[:event_sync]}"
  end
end
