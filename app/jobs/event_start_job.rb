class EventStartJob < ApplicationJob
  queue_as :critical

  def perform(event_id)
    event = Event.find(event_id)
    starting_time = event.starting_time
    duration = event.duration_from_recordings
    if  -1.minutes.from_now < starting_time and starting_time < 1.minutes.from_now
      ActionCable.server.broadcast("event_sync_#{event_id}","start")
      EventSyncJob.set(wait: 30.seconds).perform_later(event_id, starting_time.to_i, duration)
    end
  end
end
