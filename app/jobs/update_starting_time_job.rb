class UpdateStartingTimeJob < ApplicationJob
  queue_as :default

  def perform(event_id, starting_time)
    event = Event.find(event_id)
    new_start = Time.at(starting_time).to_datetime
    event.starting_time = new_start
    event.save
  end
end

