class EventSyncJob < ApplicationJob
  queue_as :default

  def perform(event_id, starting_time, duration)
    now = Time.zone.now.to_i
    passed = now - Integer(starting_time)
    ActionCable.server.broadcast("event_sync_#{event_id}", passed)
    if passed < duration + 20
     EventSyncJob.set(wait: 30.seconds).perform_later(event_id, starting_time, duration)
    else
      event = Event.find(event_id)
      unless event.repeat_days.blank?
        weekdays = %w(Sonntag Montag Dienstag Mittwoch Donnerstag Freitag Samstag)
        (1..7).each { |i|
          next_weekday = (Time.zone.now.wday + i) % 7
          if event.repeat_days.include? weekdays[next_weekday]
            UpdateStartingTimeJob.set(wait: 30.minutes).perform_later(event_id, starting_time + i.days.to_i)
            break
          end
        }
      end
    end
  end
end

