class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :configure_devise_permitted_parameters, if: :devise_controller?

  protected

  def after_sign_in_path_for(resource)
    session[:previous_url] || root_path
  end

  def configure_devise_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up,        keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
  end

  def deny_request
    render :file => 'public/403.html', :status => :forbidden, :layout => false
  end

  def deny_json_request
    render json: { errors: 'ssl required' }, :status => :forbidden
  end

  def ssl_configured?
    Rails.env.production? and not request.ssl?
  end

  def authenticate_api_key!
    keys = ApiKey.find_by key: params['api_key']
    redirect_to admin_dashboard_path if keys.nil?
  end
 end
