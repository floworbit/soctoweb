module Frontend
  class ModerationController < ApplicationController
    before_action :authorize_admin

    def authorize_admin
      redirect_to root_path, alert: 'Access Denied' unless current_admin_user
    end

    def mute
      event_message = EventMessage.find(params[:id])
      user = event_message.user
      user.update_column(:mute_reason, params.dig(:user, :mute_reason))
      user.update_column(:muted_until, params.dig(:user, :muted_until))
      redirect_to "/admin/message_reports"
    end

    def mute_menu
      @event_message = EventMessage.find(params[:id])
      @user = @event_message.user
    end

    def unmute_menu
      user = User.find(params[:id])
      user.update_column(:muted_until, nil)
      redirect_to "/admin/users"
    end

    def context
      @context_messages = EventMessage.where("id < #{params[:id].to_i + 15} AND id > #{params[:id].to_i - 15} AND event_id = #{EventMessage.find(params[:id]).event_id}")
    end

    def delete
      report = MessageReport.find_by!(id: params[:id])
      MessageReport.where(event_message: report.event_message).destroy_all
      report.event_message.destroy
      redirect_back(fallback_location: "/admin/message_reports")
    end

    def approve
      report = MessageReport.find_by!(id: params[:id])
      MessageReport.where(event_message: report.event_message).destroy_all
      redirect_back(fallback_location: "/admin/message_reports")
    end
  end
end
