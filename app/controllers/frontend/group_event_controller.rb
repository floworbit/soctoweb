module Frontend
  class GroupEventController < FrontendController
    before_action :load_event
    before_action :authenticate_user!

    def create
      orig_event_id = params.dig(:group_event, :event_id)
      source_event = Event.find(orig_event_id)
      new_event = GroupEvent.new(source_event.attributes.select{ |key, _| GroupEvent.attribute_names.include? key and !(%w(id, guid, created_at, updated_at).include? key)})
      date = Date.strptime(params.dig(:group_event, :date), "%d.%m.%Y")
      new_event.starting_time = Time.new(date.year, date.month, date.day, params.dig(:group_event, :hours), params.dig(:group_event, :minutes))
      new_event.join_code = SecureRandom.alphanumeric(9)
      new_event.synch_event = true
      new_event.original_event = orig_event_id
      new_event.slug = new_event.slug + "-" + new_event.join_code
      new_event.save

      GroupMember.create(event_id: new_event.id, user_id: current_user.id)

      render "/group_event"
    end

    def join
      code = params.dig(:group_event, :join_code).strip
      event = GroupEvent.find_by(join_code: code)
      if event.present?
        GroupMember.find_or_create_by(event_id: event.id, user_id: current_user.id)
      end
    end

    def load_event
      @event = GroupEvent.find_by!(slug: params[:slug])
    end

  end
end

