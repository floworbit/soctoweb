module Frontend
  class AccountController < FrontendController
    before_action :authenticate_user!

    #GET /settings/blocks
    def blocks
      @blocks = Block.where(user_id: current_user.id)
      respond_to { |format| format.html }
    end

    #GET /settings/gdpr-download
    def gdpr_download
      send_data current_user.to_csv, filename: "#{current_user.name}-#{Date.today}.csv", :type => "text/csv", :disposition => "attachment"
    end

    #GET /library
    def library
      @events = Frontend::Event.joins("
INNER JOIN purchases
ON events.conference_id = purchases.conference_id
AND events.supportable = TRUE
AND purchases.finalized = TRUE
AND purchases.user_id = '#{current_user.id}'
")

      respond_to { |format| format.html }
    end
  end
end
