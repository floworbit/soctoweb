module Frontend
  class BlockController < FrontendController
    before_action :authenticate_user!

    def create
      targetId = EventMessage.find(params[:id]).user_id
      if !Block.exists?(user_id: current_user.id, target: targetId)
        @block = Block.create(
            user_id: current_user.id,
            target: targetId
        )
      else
        head 409
      end
    end

    def getblocks
      render :json => Block.where(user_id: current_user.id)
    end

    def destroy
      block = Block.find(params[:id])
      if block.user_id == current_user.id
        block.destroy
        redirect_back(fallback_location: "/settings/blocks")
      else
        head 403
      end
    end

  end
end
