module Frontend
  require 'paypal-sdk-rest'
  class PurchaseController < FrontendController
    before_action :authenticate_user!

    include PayPal::SDK::REST
    PayPal::SDK::Core::Config.load('config/paypal.yml',  ENV['RACK_ENV'] || 'development')
    PayPal::SDK.logger.level = Logger::INFO

    def create
      paid_float = params[:purchase][:paid].sub(",",".").to_f
      paid_cents = (paid_float * 100).to_int
      purchase = Purchase.new(paid: paid_cents, conference_id: params[:purchase][:conference_id], event_id: params[:purchase][:event_id])
      event = Event.find(purchase.event_id)
      if paid_cents < event.view_threshold
        flash[:alert] = "Du musst mindestens " + event.view_threshold_pretty + "€ zahlen um diesen Inhalt ansehen zu können."
        redirect_back(fallback_location: root_path)
      else
        purchase.user_id = current_user.id
        purchase.watermark = current_user.id
        if Event.where(conference_id: params[:purchase][:conference_id]).where("#{Event.table_name}.download_threshold < ?", paid_cents).exists?
          purchase.download_access = true
        end

        @payment = Payment.new({
          :intent =>  "sale",
          :payer =>  {
            :payment_method =>  "paypal" },
          :redirect_urls => {
            :return_url => "#{request.base_url}/purchase",
            :cancel_url => "#{request.base_url}/v/#{event.slug}" },
          :transactions =>  [{
            :item_list => {
              :items => [{
               :name => event.title,
               :sku => "#{current_user.id}+#{event.id}",
               :price => paid_float,
               :currency => "EUR",
               :quantity => 1 }]},
            :amount =>  {
              :total =>  paid_float,
              :currency =>  "EUR" },
            :description =>  "#{event.title} auf dok.watch" }]})

        if @payment.create
          @redirect_url = @payment.links.find{|v| v.rel == "approval_url" }.href
          logger.info "Payment[#{@payment.id}]"
          logger.info "Redirect: #{@redirect_url}"
          purchase.paypal_id = @payment.id
          purchase.save
          redirect_to @redirect_url
        else
          logger.error @payment.error.inspect
        end
      end
    end

    def execute
      payment = Payment.find(params[:paymentId])
      purchase = Purchase.find_by(paypal_id: params[:paymentId])
      if payment.execute(:payer_id => params[:PayerID])
        purchase.update(finalized: true)
      else
        set_flash_message!(:alert, payment.error)
      end
      redirect_to "/v/#{Event.find(purchase.event_id).slug}"
    end
  end
end