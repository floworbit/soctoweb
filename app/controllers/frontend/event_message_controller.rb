module Frontend
  class EventMessageController < FrontendController
    before_action :load_entities

    def create
      if current_user.muted_until == nil or current_user.muted_until < Time.zone.now
        @event_message = EventMessage.create(
          user: current_user,
          name: current_user.name,
          event: @event,
          message: params.dig(:event_message, :message))
        ActionCable.server.broadcast("event_#{@event.id}", @event_message)
      end
    end

    protected

    def load_entities
      @event = Event.find params.dig(:event_message, :event_id)
    end
  end
end