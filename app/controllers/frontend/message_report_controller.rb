module Frontend
  class MessageReportController < FrontendController
    before_action :authenticate_user!
    def create
      @message_report = MessageReport.create(event_message: EventMessage.find(params[:id]));
    end
  end
end