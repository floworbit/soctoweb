class ContentController < ApplicationController
  def load
    key = ViewKey.find_by(key: params[:key])
    path = "/mnt/private/" + params[:path] + "." + params[:format]
    path = File.expand_path(path)

    if key.nil? or
        (!key.user_id.nil? and key.user_id != current_user.id) or
        File.basename(path) != key.filename or
        !File.dirname(path).start_with?("/mnt/private") or
        key.created_at < (Time.zone.now - 4.hours)
      head 403
    else
      disposition = request.xhr? ? 'inline' : 'attachment'
      send_file(path, x_sendfile: true, disposition: disposition)
    end
  end
end