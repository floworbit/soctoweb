# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  # before_action :configure_sign_up_params, only: [:create]
  # before_action :configure_account_update_params, only: [:update]

  # GET /resource/sign_up
  # def new
  #  super
  # end

  # POST /resource/sign_in
  # def create
  #   self.resource = warden.authenticate!(auth_options)
  #  sign_in(resource_name, resource)
  #  yield resource if block_given?
  #  respond_with resource, location: after_sign_in_path_for(resource)
  # end

  # POST /resource
  def create
    build_resource(sign_up_params)

    if params[:agb].present?
      resource.save
      yield resource if block_given?
      if resource.persisted?
        if resource.active_for_authentication?
          set_flash_message! :notice, :signed_up
          sign_up(resource_name, resource)
          respond_with resource, location: after_sign_up_path_for(resource)
        else
          set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
          expire_data_after_sign_in!
          respond_with resource, location: after_inactive_sign_up_path_for(resource)
        end
      else
        clean_up_passwords resource
        set_minimum_password_length
        respond_with resource
      end
    else
      resource.errors.add(:error, "- Um dich zu registrieren akzeptiere bitte die AGB und die Datenschutzerklärung.")
      respond_with resource
    end
  end

  # GET /resource/edit
  # def edit
  #    super
  # end

  # PUT /resource
  # def update
  #   super
  # end

  # DELETE /resource
  def destroy
    old_name = resource.name
    old_id = resource.id
    if resource.destroy_with_password(params[:user][:current_password])
      if ArchivedName.where(name: old_name).blank?
        ArchivedName.create(name: old_name, user_id: old_id)
      end
      flash[:notice] = "Dein Account wurde gelöscht."
      redirect_to root_path
    else
      render "users/registrations/edit"
    end
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
  end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [:name])
  end

  def update_resource(resource, params)
    if params.has_key?(:name)
      update_nickname(resource, params)
    else
      params[:name] = current_user.name
      resource.update_with_password(params)
    end
  end

  def update_nickname(resource, params)
    # TODO Rate limit
    if ArchivedName.where(name: params[:name]).blank? or !(ArchivedName.where(name: params[:name], user_id: resource.id).blank?)
      old_name = resource.name
      result = resource.update_without_password(params)
      if result and ArchivedName.where(name: old_name).blank?
        ArchivedName.create(name: old_name, user_id: resource.id)
      end
    else
      resource.errors.add(:name, :taken)
    end
  end

  # The path used after sign up.
  # def after_sign_up_path_for(resource)
  #   super(resource)
  # end

  def after_update_path_for(resource)
    "/settings"
  end

  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
end